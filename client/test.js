const citationTest=
    {
      "quote": "J'ai connu une polonaise qui en prenait au petit déjeuner... \nFaut quand même admettre que c'est plutôt une boisson d'homme.\n",
      "character": "Fernand Naudin",
      "image": "https://media.senscritique.com/media/000019334382/960/Les_Tontons_flingueurs.jpg",
      "characterDirection": "Right",
      "origin": "Les tontons flingueurs",
      "addedBy": "p132456789",
      "scores": {
        "6059c14d617ca98251240091": {
          "wins": 5,
          "looses": 2
        },
        "6058567c999f8d1643f84c14": {
          "wins": 0,
          "looses": 3
        }
      },  
      "_id": "6058567c999f8d1643f84c08"
    };
const citationSansScore=
      {
        "quote": "J'ai connu une polonaise qui en prenait au petit déjeuner... \nFaut quand même admettre que c'est plutôt une boisson d'homme.\n",
        "character": "Fernand Naudin",
        "image": "https://media.senscritique.com/media/000019334382/960/Les_Tontons_flingueurs.jpg",
        "characterDirection": "Right",
        "origin": "Les tontons flingueurs",
        "_id": "6058567c999f8d1643f84c08"
      };


suite("Test des fonctions fetch", function() {
  test("Test de la fonction fecthCitation", function(done){
    fetchCitations()
      .then(response => {
        chai.assert.isArray(response);
        chai.assert.isObject(response[0]);
        chai.assert.isString(response[0].quote);
        done();
      })
      .catch(err => {
        done(err);
      })
  });
});


suite("Tests pour la fonction getRandomInteger",function() { 
  test("On vérifie que le résultat est un entiers",function() { 
    chai.assert.isNumber(getRndInteger(5,2));
  });
  
  test("On vérifie que le résultat est bien entre 0 et max-1", function() {
    const resultat_attendu=0;
    chai.assert.deepEqual(getRndInteger(1,-1), resultat_attendu);
  });

        
  test("On vérifie que exclude est bien exclut du résultat", function() {
    const resultat_attendu = 1;
    chai.assert.deepEqual(getRndInteger(2,0), resultat_attendu);
  });
});


suite("Tests pour la fonction orienterPersonnage",function(){
  test("On renvoie l'effet miroir si besoin",function(){
      const resultat_attendu=' style="transform: scaleX(-1);"';
      chai.assert.deepEqual(orienterPersonnage(citationTest,"Right"),resultat_attendu);
  })

  test("On ne renvoie rien si pas besoin",function(){
      const resultat_attendu='';
      chai.assert.isUndefined(orienterPersonnage(citationTest,"Left"));
  })
});


suite("Tests pour la fonction sommeVictoire",function(){
  test("Le calcul du score est correct",function(){
      const resultat_attendu=5;
      chai.assert.deepEqual(sommeVictoire(citationTest),resultat_attendu);
  })

  test("Le score vaut 0 si il n'est pas définit",function(){
    
      const resultat_attendu=0;
      chai.assert.deepEqual(sommeVictoire(citationSansScore),resultat_attendu);
  })
});

suite("Tests pour la fonction ratioVictoire",function(){
  test("Le calcul du score sans normaliser est correct",function(){
      const resultat_attendu=-4/7;
      console.log(resultat_attendu);
      chai.assert.deepEqual(ratioVictoire(citationTest,false),resultat_attendu);
  })

  test("Le calcul du score en normalisant est correct",function(){
      const resultat_attendu=-4/14;
      console.log(resultat_attendu);
      chai.assert.deepEqual(ratioVictoire(citationTest,true),resultat_attendu);
  })

  test("Le score vaut 0 si il n'est pas définit",function(){
      const resultat_attendu=0;
      chai.assert.deepEqual(ratioVictoire(citationSansScore,true),resultat_attendu);
  })
});

suite("Tests pour la fonction ajoutSomme",function(){
  test("L'ajout des points à la citation est correct",function(){
      const resultat_attendu=5;
      const citationUp = ajoutSomme(citationTest);
      chai.assert.deepEqual(citationUp.points,resultat_attendu);
  })
});

suite("Tests pour la fonction ajoutRatio",function(){
  test("L'ajout des points à la citation est correct",function(){
      const resultat_attendu=-4/7;
      const citationUp = ajoutRatio(citationTest);
      chai.assert.deepEqual(citationUp.points,resultat_attendu);
  })
});

suite("Tests pour la fonction ajoutRatioNormaliser",function(){
  test("L'ajout des points à la citation est correct",function(){
      const resultat_attendu=-4/14;
      const citationUp = ajoutRatioNormaliser(citationTest);
      chai.assert.deepEqual(citationUp.points,resultat_attendu);
  })
});

suite("Tests pour la fonction compareNbPoints",function(){
  test("La comparaison est correcte",function(){
    const citationTest1=
    {"id": "1", "character": "Nelson", "quote": "They taste like burning", "points": 15};
    const citationTest2= {"id": "2", "character": "Homer", "quote": "Jsp quoi", "points": 10};
      const res = (compareNbPoints(citationTest1,citationTest2)<0) ;
      chai.assert.isTrue(res);
  })
});

suite("Tests pour la fonction whatDirection (droite)",function(){
  test("La direction renvoyée est correcte",function(){
      const resultat_attendu ="Right" ;
      chai.assert.deepEqual(whatDirection(true),resultat_attendu);
  })

  test("La direction renvoyée est correcte (gauche)",function(){
    const resultat_attendu ="Left" ;
    chai.assert.deepEqual(whatDirection(false),resultat_attendu);
})
});

