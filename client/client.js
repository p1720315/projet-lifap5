/* ******************************************************************
 * Constantes de configuration
 */
const apiKey = "10735eee-ae77-488b-92df-c7726fb95b4f";
const serverUrl = "https://lifap5.univ-lyon1.fr";

/* *****************************************************************
 * fonctionnalités à implémenter
 ******************************************************************* */

/**
 * (Callback) Fonction somme pour la méthode reduce()
 * @param {any} acc accumulateur 
 * @param {any} val valeur courante
 * @returns somme de acc et val
 */
function sumReduce(acc,val){
  return acc+val;
}


/* ******************************************************************
 * Récupération du tableau de citation
 ******************************************************************** */

/**
 * Fait une requête GET authentifiée sur /citations
 * @returns une promesse du tableau de citations ou du message d'erreur
 */
function fetchCitations() {
  //console.log("CALL fetchCitations");
  return fetch(serverUrl + "/citations", { headers: { "x-api-key": apiKey } })
    .then((response) => response.json())
    .then((jsonData) => {
      if (jsonData.status && Number(jsonData.status) != 200) {
        return { err: jsonData.message };
      }
      return jsonData;
    })
    .catch((erreur) => ({ err: erreur }));
}

/**
 * Fait une requête sur le serveur, met à jour l'etat courant 
 * avec le tableau récupéré, puis appele une fonction qui choisira
 * au hasard deux citations pour le duel.
 * @param {Etat} etatCourant l'état courant
 */
function lanceCitations(etatCourant) {
  //console.log("CALL lanceCitations");
  fetchCitations().then((data) => {
    etatCourant.citations=data;
    chooseTwoRandom(etatCourant);   
  });
}


/* ******************************************************************
 * Choix de d'un duel aléatoire
 ******************************************************************** */

/**
 * Renvoie un entier aléatoire entre 0 et max-1, différent de exclude
 * @param {Number} max maximum exclut pour le tirage
 * @param {Number} exclude nombre à exclure lors du tirage
 * @returns un nombre aléatoire
 */
 function getRndInteger(max,exclude) {
  //console.log("CALL getRndInteger");
  const n=Math.floor(Math.random() * max);
  if (n===exclude) {
    return getRndInteger(max,exclude);
  }
  return n;
}

/**
 * Met à jour l'état courant en choisissant 2 citations aléatoire
 * @param {Etat} etatCourant l'état courant
 */
function chooseTwoRandom(etatCourant){
  //console.log("CALL chooseTwoRandom");
  etatCourant.idcapitalist=getRndInteger(etatCourant.citations.length,-1);
  etatCourant.idcommunist=getRndInteger(etatCourant.citations.length,etatCourant.idcapitalist);
  majPage(etatCourant);
}


/* ******************************************************************
 * Affichage de l'ensemble des citations du serveur
 ******************************************************************** */

/**
 * (Callback) Transforme une citation en une ligne de tableau HTML
 * @param {Citation} citation la citation à transformer en HTML
 * @param {Number} indice l'indice de la citation dans le tableau de citations
 * @returns une chaine caractère contenant la citation sous forme HTML
 */
function citationToHTML(citation,indice){
  return '<tr><td>'+(indice+1)+'</td><td>'+citation.character+'</td><td>'+citation.quote+'</td><td>'+citation.points+'</td></tr>';
}


/**
 * Transforme le tableau de citations en un tableau HTML
 * @param {Array} citations le tableau de citations 
 * @returns une chaine caractère sous forme de tableau HTML
 */
function citationsToTable(citations) {
  //console.log("CALL citationsToTable");
  return citations.map(citationToHTML).reduce(sumReduce,'');
}


/**
 * Remplit le code HTML du tableau de citations dans index.html
 * @param {Array} citations le tableau de citations 
 */
function remplirTabCitation(citations) {
  //console.log("CALL remplirTabCitation");
  document.getElementById("tableau-tri").innerHTML='<table class="table"><thead><tr><th>Classement</th><th>Personnage</th><th>Citation</th><th>Score</th></tr></thead><tbody>'+ citationsToTable(citations) +'</tbody></table>';
}


/* ******************************************************************
 * Affichage du duel choisit
 ******************************************************************** */

/**
 * Si le personnage est mal orienté, rajoute un effet miroir au code HTML
 * @param {Citation} citation la citation à orienter 
 * @param {string} side le coté où se trouve la citation 
 * @returns une chaine de caractère HTML avec effet miroir
 */
function orienterPersonnage(citation,side){
  //console.log("CALL orienterPersonnage");
  if (citation.characterDirection==side){
    return ' style="transform: scaleX(-1);"'
  }
}

/**
 * Remplit le code HTML du duel de citations dans index.html
 * @param {Citation} citationCapitalist la citation de droite
 * @param {Citation} citationCommunist la citation de gauche
 */
function remplirTabVote(citationCapitalist,citationCommunist) {
  //console.log("CALL remplirTabVote");
  document.getElementById("image-droite").innerHTML='<img src="'+citationCapitalist.image+orienterPersonnage(citationCapitalist,"Right")+'" />';
  document.getElementById("title-droite").innerHTML=citationCapitalist.quote
  document.getElementById("subtitle-droite").innerHTML=citationCapitalist.character+' dans '+citationCapitalist.origin;
  document.getElementById("image-gauche").innerHTML='<img src="'+citationCommunist.image+orienterPersonnage(citationCommunist,"Left")+'" />';
  document.getElementById("title-gauche").innerHTML=citationCommunist.quote;
  document.getElementById("subtitle-gauche").innerHTML=citationCommunist.character+' dans '+citationCommunist.origin;

}

/* ******************************************************************
 * Points selon la méthode asbolue
 ******************************************************************** */

/**
 * Calcule la somme des victoires d'une citations
 * @param {Citation} citation la citation dont on veut connaitre le score 
 * @returns la somme des victoires
 */
function sommeVictoire(citation){
  //console.log("scores is undefined :"+ (citation.scores===undefined));
  if (citation.scores!==undefined){  
    return Object.values(citation.scores)
    .map((data) => data.wins)
    .reduce(sumReduce);  
  }
  else{ 
    return 0
  }
}

/**
 * (Callback) Met à jour le nombre de points de la citation en sommant ses victoires
 * @param {Citation} citation la citation à mettre à jour 
 * @returns la citation mise à jour
 */
function ajoutSomme(citation){
  citation.points=sommeVictoire(citation);
  return citation;
}

/* ******************************************************************
 * Points selon la méthode ratio
 ******************************************************************** */

/**
 * Calcule le ratio de victoires d'une citations
 * @param {Citation} citation la citation dont on eut connaitre le score 
 * @param {boolean} normaliser normaliser le résultat par le nombre d'adversaire
 * @returns la somme des victoires
 */
function ratioVictoire(citation,normaliser){
  //console.log("scores is undefined :"+ (citation.scores===undefined));
  if (citation.scores!==undefined){
    const tableaupoints=Object.values(citation.scores);
    const points=tableaupoints
    .map((data) => ((data.wins-data.looses)/(data.wins+data.looses)))
    .reduce(sumReduce);
    if(normaliser){return points/tableaupoints.length;}
    else{return points;}
  }
  else{ 
    return 0
  }
}

/**
 * (Callback) Met à jour le nombre de points de la citation avec le ratio de victoires
 * @param {Citation} citation la citation à mettre à jour 
 * @returns la citation mise à jour
 */
function ajoutRatio(citation){
  citation.points=ratioVictoire(citation,false);
  return citation;
}

/**
 * (Callback) Met à jour le nombre de points de la citation avec le ratio de victoires normalisé par le nombre d'adversaires
 * @param {Citation} citation la citation à mettre à jour
 * @returns la citation mise à jour
 */
function ajoutRatioNormaliser(citation){
  citation.points=ratioVictoire(citation,true);
  return citation;
}

/* ******************************************************************
 * Tri des citations en fonction du classement
 ******************************************************************** */

/**
 * (Callback) Classe 2 citations en fonction de leur nombre de points
 * @param {Citation} citation1 une citation
 * @param {Citation} citation2 une autre citation
 * @returns >0 si citation1 plus grande, <0 si citation2 plus grande, 0 si égales 
 */
function compareNbPoints(citation1,citation2){
  return (citation2.points-citation1.points);
}

/**
 * Classe les citations en fonction du mode choisit, et appele les fonctions d'affichage
 * @param {Etat} etatCourant l'état courant
 */
function classeCitations(etatCourant){
  //console.log("CALL classeCitations");
  if (etatCourant.tri==="ratio"){
    if(etatCourant.normaliser){
      remplirTabCitation(etatCourant.citations.map(ajoutRatioNormaliser).sort(compareNbPoints));
    }
    else{
      remplirTabCitation(etatCourant.citations.map(ajoutRatio).sort(compareNbPoints));
    }
  } 
  else{
    remplirTabCitation(etatCourant.citations.map(ajoutSomme).sort(compareNbPoints));
  }
  remplirTabVote(etatCourant.citations[etatCourant.idcapitalist],etatCourant.citations[etatCourant.idcommunist]);
}


/* ******************************************************************
 * Vote pour une citation
 ******************************************************************** */

/**
 * Fait une requete POST sur citations/duels
 * @param {string} _idwinner _id de la citation gagnante
 * @param {string} _idloser _id de la citation perdante 
 * @returns une promesse des 2 citations mise à jour
 */
function fetchVote(_idwinner,_idloser){
  //console.log("CALL fetchVote");
  return fetch(serverUrl + "/citations/duels", { 
    method: "POST",
    headers: { "x-api-key": apiKey, "Content-Type":"application/json" },
    body: JSON.stringify({
      "winner":_idwinner,
      "looser":_idloser
    })}).then((response) => response.json());
}

/**
 * Fais une requete sur le serveur et met à jour les citations du duel
 * @param {Etat} etatCourant l'état courant
 * @param {Number} idWinner l'indice de la citation gagnante
 * @param {Number} idLoser l'indice de la citation perdante
 */
function voteCitation(etatCourant,idWinner,idLoser){
  //console.log("CALL voteCitation");
  fetchVote(etatCourant.citations[idWinner]._id,etatCourant.citations[idLoser]._id).then(
    (data) => { 
      etatCourant.citations[idWinner]=data[0];
      etatCourant.citations[idLoser]=data[1];
      chooseTwoRandom(etatCourant);
    });
}


/* ******************************************************************
 * Gestion du mode de classement et des actions enclenchées par les boutons
 ******************************************************************** */

/**
 * Rend actif le mode de tri modeTri
 * @param {string} modeTri le mode de classement que l'on veut utiliser
 * @param {Etat} etatCourant l'état courant 
 */
function choixTri(modeTri,etatCourant){
  const btnAbsolu=document.getElementById("btn-absolu");
  const btnRatio=document.getElementById("btn-ratio");
  const btnNormaliser=document.getElementById("btn-normaliser");
  if (modeTri==="absolu"){
    btnAbsolu.setAttribute("disabled","");
    btnAbsolu.setAttribute("class","button is-success");
    btnRatio.removeAttribute("disabled");
    btnRatio.setAttribute("class","button");
    btnNormaliser.setAttribute("style","display: none;");
  }
  else {
    btnRatio.setAttribute("disabled","");
    btnRatio.setAttribute("class","button is-success");
    btnAbsolu.removeAttribute("disabled");
    btnAbsolu.setAttribute("class","button");
    btnNormaliser.removeAttribute("style");
  }
  etatCourant.tri=modeTri;
  majPage(etatCourant);
}

/**
 * Rend actif/inactif la normalisation pour le classement par ratio de victoire
 * @param {Etat} etatCourant l'état courant
 */
function choixNormaliser(etatCourant){
  const btnNormaliser=document.getElementById("btn-normaliser");
  if(etatCourant.normaliser){
    btnNormaliser.setAttribute("class","button is-danger");
    etatCourant.normaliser=false;
  }
  else{
    btnNormaliser.setAttribute("class","button is-success");
    etatCourant.normaliser=true;
  }
  majPage(etatCourant);
}

/**
 * Enregistre les actions à effectuer lorsque l'on clique sur les bonton 
 * (choix du mode de classement et vote)
 * @param {Etat} etatCourant l'état courant 
 */
function actionBtn(etatCourant) {
  document.getElementById("vote-capitaliste").onclick = () => 
    voteCitation(etatCourant,etatCourant.idcapitalist,etatCourant.idcommunist);
  document.getElementById("vote-communiste").onclick = () => 
    voteCitation(etatCourant,etatCourant.idcommunist,etatCourant.idcapitalist);
  document.getElementById("btn-absolu").onclick = () =>
    choixTri("absolu",etatCourant);
  document.getElementById("btn-ratio").onclick = () =>
    choixTri("ratio",etatCourant);
  document.getElementById("btn-ajout").onclick = () =>
    ajouterCitation(etatCourant);
  document.getElementById("btn-normaliser").onclick = () =>
    choixNormaliser(etatCourant);
}

/* ******************************************************************
 * Gestion ajout citation
 ******************************************************************** */

/**
 * renvoie Right ou Left en fonction du booléen
 * @param {bool} dir le radio bouton regard vers la droite est cochée
 * @returns une chaine de caractère de la direction
 */
 function whatDirection(dir){
  if (dir){return "Right";}
  else{return "Left";}
}

/**
 * recupere le formulaire et appelle la fct fetch
 * @param {Etat} etatCourant etat courant 
 */
function ajouterCitation(etatCourant){
  const quote=document.getElementById("quote").value;
  const name=document.getElementById("name").value;
  const origin=document.getElementById("origin").value;
  const image=document.getElementById("image").value;
  const direction=whatDirection(document.getElementById("rightDirection").
  checked);
  if(quote!=""&&origin!=""&&name!=""){
    fetchAjouterCitation(quote,name,origin,image,direction).then(
      (data)=>{
        etatCourant.citations[etatCourant.citations.length]=data;
        majPage(etatCourant);
      }
    );
    document.getElementById("quote").value="";
    document.getElementById("name").value="";
    document.getElementById("origin").value="";
    document.getElementById("image").value="";
  }
  else{
    window.alert("LES CHAMPS CITATIONS, NOMS ET ORIGINE SONT O-BLI-GA-TOIRE!");
  }
}

/**
 * fait une requete POST sur /citations
 * @param {string} quote citation
 * @param {string} name nom du personnage
 * @param {string} origin origine de la citation
 * @param {string} image lien de l'image
 * @param {string} direction direction du regard
 * @returns une promesse de la citation ajoutée
 */
function fetchAjouterCitation(quote,name,origin,image,direction){
  //console.log("CALL fetchAjoutCitation");
  return fetch(serverUrl + "/citations", { 
    method: "POST",
    headers: { "x-api-key": apiKey, "Content-Type":"application/json" },
    body: JSON.stringify({
      "quote": quote,
       "character": name,
       "image": image,
       "characterDirection": direction,
       "origin": origin
    })}).then((response) => response.json());
}


/* ******************************************************************
 * Gestion des tabs "Voter" et "Toutes les citations"
 ******************************************************************** */

/**
 * Affiche/masque les divs "div-duel" et "div-tout"
 * selon le tab indiqué dans l'état courant.
 *
 * @param {Etat} etatCourant l'état courant
 */
function majTab(etatCourant) {
  //console.log("CALL majTab");
  const dDuel = document.getElementById("div-duel");
  const dTout = document.getElementById("div-tout");
  const dAjout = document.getElementById("div-ajout");
  const tDuel = document.getElementById("tab-duel");
  const tTout = document.getElementById("tab-tout");
  const tAjout = document.getElementById("tab-ajout");
  if (etatCourant.tab === "duel") {
    dDuel.style.display = "flex";
    tDuel.classList.add("is-active");
    dTout.style.display = "none";
    tTout.classList.remove("is-active");
    dAjout.style.display = "none";
    tAjout.classList.remove("is-active");
  } else {
    if (etatCourant.tab==="ajout"){
      dAjout.style.display = "flex";
      tAjout.classList.add("is-active");
      dTout.style.display = "none";
      tTout.classList.remove("is-active");
      dDuel.style.display = "none";
      tDuel.classList.remove("is-active");
    }
    else{
      dTout.style.display = "flex";
      tTout.classList.add("is-active");
      dDuel.style.display = "none";
      tDuel.classList.remove("is-active");
      dAjout.style.display = "none";
      tAjout.classList.remove("is-active");
    }
  }
}

/**
 * Mets au besoin à jour l'état courant lors d'un click sur un tab.
 * En cas de mise à jour, déclenche une mise à jour de la page.
 *
 * @param {String} tab le nom du tab qui a été cliqué
 * @param {Etat} etatCourant l'état courant
 */
function clickTab(tab, etatCourant) {
  //console.log(`CALL clickTab(${tab},...)`);
  if (etatCourant.tab !== tab) {
    etatCourant.tab = tab;
    majPage(etatCourant);
  }
}

/**
 * Enregistre les fonctions à utiliser lorsque l'on clique
 * sur un des tabs.
 *
 * @param {Etat} etatCourant l'état courant
 */
function registerTabClick(etatCourant) {
  //console.log("CALL registerTabClick");
  document.getElementById("tab-duel").onclick = () =>
    clickTab("duel", etatCourant);
  document.getElementById("tab-tout").onclick = () =>
    clickTab("tout", etatCourant);
  document.getElementById("tab-ajout").onclick = () =>
    clickTab("ajout", etatCourant);
}

/* ******************************************************************
 * Gestion de la boîte de dialogue (a.k.a. modal) d'affichage de
 * l'utilisateur.
 * ****************************************************************** */

/**
 * Fait une requête GET authentifiée sur /whoami
 * @returns une promesse du login utilisateur ou du message d'erreur
 */
function fetchWhoami() {
  //console.log("CALL fetchWhoami")
  return fetch(serverUrl + "/whoami", { headers: { "x-api-key": apiKey } })
    .then((response) => response.json())
    .then((jsonData) => {
      if (jsonData.status && Number(jsonData.status) != 200) {
        return { err: jsonData.message };
      }
      return jsonData;
    })
    .catch((erreur) => ({ err: erreur }));
}

/**
 * Fait une requête sur le serveur et insère le login dans
 * la modale d'affichage de l'utilisateur.
 * @returns Une promesse de mise à jour
 */
function lanceWhoamiEtInsereLogin() {
  return fetchWhoami().then((data) => {
    const elt = document.getElementById("elt-affichage-login");
    const ok = data.err === undefined;
    if (!ok) {
      elt.innerHTML = `<span class="is-error">${data.err}</span>`;
    } else {
      elt.innerHTML = `Bonjour ${data.login}.`;
    }
    return ok;
  });
}

/**
 * Affiche ou masque la fenêtre modale de login en fonction de l'état courant.
 *
 * @param {Etat} etatCourant l'état courant
 */
function majModalLogin(etatCourant) {
  const modalClasses = document.getElementById("mdl-login").classList;
  if (etatCourant.loginModal) {
    modalClasses.add("is-active");
    lanceWhoamiEtInsereLogin(etatCourant);
  } else {
    modalClasses.remove("is-active");
  }
}

/**
 * Déclenche l'affichage de la boîte de dialogue du nom de l'utilisateur.
 * @param {Etat} etatCourant
 */
function clickFermeModalLogin(etatCourant) {
  etatCourant.loginModal = false;
  majPage(etatCourant);
}

/**
 * Déclenche la fermeture de la boîte de dialogue du nom de l'utilisateur.
 * @param {Etat} etatCourant
 */
function clickOuvreModalLogin(etatCourant) {
  etatCourant.loginModal = true;
  majPage(etatCourant);
}

/**
 * Enregistre les actions à effectuer lors d'un click sur les boutons
 * d'ouverture/fermeture de la boîte de dialogue affichant l'utilisateur.
 * @param {Etat} etatCourant
 */
function registerLoginModalClick(etatCourant) {
  document.getElementById("btn-close-login-modal1").onclick = () =>
    clickFermeModalLogin(etatCourant);
  document.getElementById("btn-close-login-modal2").onclick = () =>
    clickFermeModalLogin(etatCourant);
  document.getElementById("btn-open-login-modal").onclick = () =>
    clickOuvreModalLogin(etatCourant);
}

/* ******************************************************************
 * Initialisation de la page et fonction de mise à jour
 * globale de la page.
 * ****************************************************************** */

/**
 * Mets à jour la page (contenu et événements) en fonction d'un nouvel état.
 *
 * @param {Etat} etatCourant l'état courant
 */
function majPage(etatCourant) {
  //console.log("CALL majPage");
  console.log(etatCourant);
  majTab(etatCourant);
  majModalLogin(etatCourant);
  registerTabClick(etatCourant);
  registerLoginModalClick(etatCourant);
  actionBtn(etatCourant);
  classeCitations(etatCourant);
 }

/**
 * Appelé après le chargement de la page.
 * Met en place la mécanique de gestion des événements
 * en lançant la mise à jour de la page à partir d'un état initial.
 */
function initClientCitations() {
  //console.log("CALL initClientCitations");
  const etatInitial = {
    tab: "duel",
    loginModal: false,
    citations : [],
    tri: "absolu",
    normaliser: false
  };
  fetchWhoami().then((data)=>{
    const ok = data.err === undefined;
    if (ok){
      etatInitial.login=data.login;
    }
    lanceCitations(etatInitial);
  });
}

// Appel de la fonction init_client_duels au après chargement de la page
document.addEventListener("DOMContentLoaded", () => {
  //console.log("Exécution du code après chargement de la page");
  initClientCitations();
});